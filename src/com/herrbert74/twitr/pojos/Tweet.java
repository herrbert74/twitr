package com.herrbert74.twitr.pojos;

public class Tweet {
	String text;
	String author;

	public Tweet(String text, String author) {
		super();
		this.text = text;
		this.author = author;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String from) {
		this.author = from;
	}
}
