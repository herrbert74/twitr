package com.herrbert74.twitr;

public interface TwitrConstants {

	// Prefs files - NOT USED
	public static final String PREFS_MAIN = "mainPrefs";
	
	//URLs
	static final String URL = "https://stream.twitter.com/1/statuses/filter.json?track=";
	static final String SEARCHTERM = "banking";
	
	//Typefaces
	static final String TYPEFACE_HEADINGS = "kimberley bl.otf";
	static final String TYPEFACE_BODY = "AlteHaasGroteskBold.ttf";

	}