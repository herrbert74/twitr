package com.herrbert74.twitr.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.herrbert74.twitr.R;
import com.herrbert74.twitr.TwitrApp;
import com.herrbert74.twitr.TwitrConstants;
import com.herrbert74.twitr.adapters.TweetsArrayAdapter;
import com.herrbert74.twitr.dialogs.CredentialsDialog;
import com.herrbert74.twitr.pojos.Tweet;

public class MainActivity extends FragmentActivity implements TwitrConstants, CredentialsDialog.ReadyListener {

	private ArrayList<Tweet> mTweets = new ArrayList<Tweet>();
	private ArrayAdapter<Tweet> mAdapter;
	private Button btn;
	private boolean mKeepRunning = false;
	private String mUsername, mPassword;
	Thread mThread;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TextView lbl_title = (TextView) findViewById(R.id.lbl_title);
		btn = (Button) findViewById(R.id.btn_start_stop);
		lbl_title.setTypeface(TwitrApp.getFontHeaders(), Typeface.BOLD);
		mAdapter = new TweetsArrayAdapter(this, mTweets);
		((ListView) findViewById(R.id.lv)).setAdapter(mAdapter);
		showCredentialsDialog();
	}

	/**
	 * Alternate switch for starting and stopping the stream with the only
	 * button on the screen
	 * 
	 * @param v
	 */
	public void startStop(View v) {
		if (((Button) v).getText().equals("Start")) {
			mThread = new StreamThread();
			mThread.start();
			mKeepRunning = true;
			((Button) v).setText("Stop");
		} else {
			mKeepRunning = false;
			stopThread(mThread);
			((Button) v).setText("Start");
		}
	}

	/**
	 * Stops the stream from outside of the thread
	 * 
	 * @param theThread
	 */
	private synchronized void stopThread(Thread theThread) {
		mKeepRunning = false;
		if (theThread != null) {
			theThread = null;
		}
	}

	/**
	 * Thread for starting the stream, processing JSON data, and sending data to
	 * the UI thread
	 * 
	 * @author Herrbert
	 * 
	 */
	private class StreamThread extends Thread {

		HttpGet request;

		@Override
		public void run() {
			try {
				DefaultHttpClient client = new DefaultHttpClient();
				Credentials creds = new UsernamePasswordCredentials(mUsername, mPassword);
				client.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), creds);
				request = new HttpGet();
				request.setURI(new URI(URL + SEARCHTERM));
				HttpResponse response = client.execute(request);
				InputStream in = response.getEntity().getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));

				parseTweets(reader);

				in.close();

			} catch (Exception e) {
				Log.e("Twitr", "Error in Thread: " + e.toString());
			}
		}

		private void parseTweets(BufferedReader reader) {

			String line = "";
			do {
				try {
					line = reader.readLine();
					final JSONObject tweet = new JSONObject(line);
					Log.d("Twitter", "Keep Running: " + mKeepRunning + " Text: " + tweet.getString("text"));
					// JSON data with text
					if (tweet.has("text")) {

						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Tweet tw;
								// get data from JSON object
								try {
									tw = new Tweet(tweet.getString("text"), tweet.getJSONObject("user").getString("screen_name"));
									mTweets.add(0, tw);
								} catch (JSONException e) {
									e.printStackTrace();
								}
								// Remove oldest tweet, so we have max 10 tweets
								if (mTweets.size() > 10) {
									mTweets.remove(mTweets.size() - 1);
								}
								mAdapter.notifyDataSetChanged();

							}
						});
					}
				} catch (JSONException e) {
					// We have an error page instead of the json result, so we
					// jump back to credentials dialog
					if (line.equals("<title>Error 401 Unauthorized</title>")) {
						runOnUiThread(new Runnable() {
							public void run() {
								if (mKeepRunning) {
									mKeepRunning = false;
									Toast.makeText(MainActivity.this, R.string.credentials_incorrect, Toast.LENGTH_LONG).show();
									showCredentialsDialog();
								}
							}
						});
					}
					Log.e("Twitr", e.getMessage());
				} catch (IOException e) {
					Log.e("Twitr", e.getMessage());
				}
				Log.d("Twitr", "in parsetweets. " + Boolean.toString(mKeepRunning) + " " + Integer.toString(line.length()));
			} while (mKeepRunning && mTweets.size() < 10); // Thread stopped or
															// reached 10 tweets
			// Stop thread, switch button
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					startStop(btn);

				}
			});
		}
	}

	/**
	 * Executed when OK button on dialog is pressed
	 * 
	 */
	@Override
	public void ready(String username, String password) {
		// Empty credentials, so show dialog again
		if (username.length() == 0 || password.length() == 0) {
			Toast.makeText(MainActivity.this, R.string.credentials_empty, Toast.LENGTH_LONG).show();
			showCredentialsDialog();
		}
		// Save credentials, start streaming
		else {
			mUsername = username;
			mPassword = password;
			startStop(btn);
		}
	}

	/**
	 * Show the dialog
	 * 
	 */
	public void showCredentialsDialog() {
		CredentialsDialog dialog = new CredentialsDialog();
		FragmentManager fm = getSupportFragmentManager();
		dialog.show(fm, "fragment");
	}

	/**
	 * Stop thread on exit
	 * 
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		stopThread(mThread);
	}
}
