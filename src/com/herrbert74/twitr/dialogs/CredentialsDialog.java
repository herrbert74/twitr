package com.herrbert74.twitr.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.herrbert74.twitr.R;
import com.herrbert74.twitr.TwitrApp;
import com.herrbert74.twitr.TwitrConstants;
import com.herrbert74.twitr.activities.MainActivity;

/**
 * The Class CredentialsDialog.
 * 
 * Used to get the credentials for the user's Twitter account.
 */

public class CredentialsDialog extends DialogFragment implements TwitrConstants {
	Context mContext;
	Button okButton;
	TextView lbl_username, lbl_password;
	EditText txt_username, txt_password;
	String message;
	
	public CredentialsDialog() {
		this.setCancelable(false);
	}

	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		View view = inflater.inflate(R.layout.dialog_credentials, container);
		
		Button btn_ok = (Button) view.findViewById(R.id.btn_ok);
		txt_username = (EditText) view.findViewById(R.id.txt_username);
		txt_password = (EditText) view.findViewById(R.id.txt_password);
		
		btn_ok.setTypeface(TwitrApp.getFontHeaders());

		btn_ok.setOnClickListener(new OKListener());

		getDialog().setTitle(R.string.credentials);
		
		return view;
		
	}

	/**
	 * The listener interface for receiving OK events. The class that is
	 * interested in processing a OK event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addOKListener<code> method. When
	 * the OK event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see OKEvent
	 */
	private class OKListener implements android.view.View.OnClickListener {

		/* (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		public void onClick(View v) {

			MainActivity activity = (MainActivity) getActivity();
            activity.ready(txt_username.getText().toString(), txt_password.getText().toString());
			CredentialsDialog.this.dismiss();
		}
	}

	/**
	 * The listener interface for receiving ready events. The class that is
	 * interested in processing a ready event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addReadyListener<code> method. When
	 * the ready event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see ReadyEvent
	 */
	public interface ReadyListener {

		/**
		 * Ready.
		 * 
		 * @param username
		 *            the username
		 * @param password
		 *            the password
		 */
		public void ready(String username, String password);
	}

}