package com.herrbert74.twitr;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

//http://www.devahead.com/blog/2011/06/extending-the-android-application-class-and-dealing-with-singleton/
//Don't forget to add a reference in the Manifest file!!!

/**
 * The Class CVApp used to store the context and sigletons like typefaces.
 */
public class TwitrApp extends Application implements TwitrConstants{

	/** The Constant TAG. */
	private static final String TAG = TwitrApp.class.getSimpleName();
	
	/** The instance. */
	private static TwitrApp instance;
	
	private static Typeface mFont_headers;
	private static Typeface mFont_body;

	/* (non-Javadoc)
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		initSingletons();
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public static Context getContext() {
		return instance.getApplicationContext();
	}

	/**
	 * Inits the singletons.
	 */
	protected void initSingletons() {
		mFont_headers = Typeface.createFromAsset(getAssets(), TYPEFACE_HEADINGS);
		mFont_body = Typeface.createFromAsset(getAssets(), TYPEFACE_BODY);
	}

	/**
	 * Gets the headers font.
	 *
	 * @return the headers font
	 */
	public static Typeface getFontHeaders() {
		return mFont_headers;
	}
	
	/**
	 * Gets the body font.
	 *
	 * @return the body font
	 */
	public static Typeface getFontBody() {
		return mFont_body;
	}
}
