package com.herrbert74.twitr.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.herrbert74.twitr.R;
import com.herrbert74.twitr.TwitrApp;
import com.herrbert74.twitr.TwitrConstants;
import com.herrbert74.twitr.pojos.Tweet;

/**
 * Creates the list of tweets
 * 
 * @author zsbertalan
 * 
 */
public class TweetsArrayAdapter extends ArrayAdapter<Tweet> implements TwitrConstants {
	private final Context mContext;
	private final ArrayList<Tweet> mTweets;
	
	public TweetsArrayAdapter(Context context, ArrayList<Tweet> tweets) {
		super(context, R.layout.adapter_tweets, tweets);
		mContext = context;
		mTweets = tweets;
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView;
		rowView = inflater.inflate(R.layout.adapter_tweets, parent, false);
		

		TextView lbl_text = (TextView) rowView.findViewById(R.id.lbl_text);
		TextView lbl_author = (TextView) rowView.findViewById(R.id.lbl_author);
		lbl_text.setTypeface(TwitrApp.getFontBody());
		lbl_author.setTypeface(TwitrApp.getFontBody());
		lbl_text.setText(mTweets.get(position).getText());
		lbl_author.setText(mTweets.get(position).getAuthor());
		return rowView;

	}
}
